package com.test.log4j2.ws.rest;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/log")
@Api(value = "Controle de test", tags = "Test")
public class TestRest {

    @RequestMapping(
            value = "/test",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public String test(){
        log.error("test error");
        log.debug("test debug");
        log.warn("test warn");
        log.info("test info");
        return "success";
    }

}
