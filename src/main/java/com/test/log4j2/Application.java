package com.test.log4j2;

import com.test.log4j2.config.SwaggerConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
@EnableAutoConfiguration
public class Application {
    public static void main (String[] args){
        new SpringApplicationBuilder(
                Application.class,
                SwaggerConfiguration.class
        ).run(args);
    }
}
